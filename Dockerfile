FROM python:3.8.2

ENV FLASK_SKIP_DOTENV=1
ENV FLASK_RUN_PORT=8000
ENV FLASK_ENV=development
ENV FLASK_RUN_HOST=0.0.0.0

WORKDIR /usr/src/app
RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .
RUN pip install --editable .

CMD worker ${FLASK_APP} default -g3
