from datetime import datetime, timedelta
import hashlib
import multiprocessing
import time

import flask

from pigeonpost import logger
from pigeonpost.decorators import job
from pigeonpost.flask_ext import PigeonPost
from pigeonpost.jobs import schedule_jobs, check_job_heartbeats
from pigeonpost.models import Job, JobSchedule, instance
from pigeonpost.worker import Worker

from unittest import TestCase


@job('default')
def healthcheck(seconds):
    time.sleep(seconds)
    return f'Healthy after {seconds} seconds'


class PigeonPostTests(TestCase):

    def setUp(self):
        self.db_id = 'test_' + hashlib.md5(self.id().encode('utf-8')).hexdigest()
        self.app = flask.Flask(__name__)
        self.app.testing = True
        self.app.config.update({
            'PIGEONPOST_MONGO_URI': 'mongodb://mongo' if self.app.env == 'development' else 'mongodb://localhost',
            'PIGEONPOST_MONGO_DB': self.db_id,
        })
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.request_context = self.app.test_request_context()
        self.request_context.push()
        PigeonPost(self.app)
        self.dropDBs()

    def tearDown(self):
        self.dropDBs()
        self.app_context.pop()

    def dropDBs(self):
        instance.db.client.drop_database(self.db_id)
        instance.db.client.drop_database(self.db_id)

    def test_worker_run(self):
        Job.collection.delete_many({})
        worker = Worker(self.app, queue_name=self.db_id, greenlets=4, max_jobs=4, burst=True)
        worker.q.client.purge()
        healthcheck.enqueue_batch([{'args': [.01], 'queue_name': self.db_id} for x in range(0, 20)])
        assert Job.collection.count_documents({'method_name': 'healthcheck', 'status': 'enqueued'}) == 20
        before_worker = datetime.utcnow()
        worker.run()
        assert Job.collection.count_documents({'method_name': 'healthcheck', 'status': 'complete'}) == 20
        assert all(j.last_heartbeat > before_worker for j in Job.find({}, projection={'last_heartbeat': 1}))

    def test_worker_sig_term_handler(self):
        Job.collection.delete_many({})
        worker = Worker(self.app, queue_name=self.db_id, greenlets=20, max_jobs=20)
        worker.q.client.purge()
        healthcheck.enqueue_batch([{'args': [.01], 'queue_name': self.db_id} for x in range(0, 40)])
        proc = multiprocessing.Process(target=worker.run)
        proc.start()
        while healthcheck.num_complete_jobs() < 20:
            time.sleep(1)
        proc.terminate()
        proc.join()
        assert not proc.is_alive()

    def test_worker_second_sig_term_handler(self):
        Job.collection.delete_many({})
        worker = Worker(self.app, queue_name=self.db_id, greenlets=20, max_jobs=20)
        worker.q.client.purge()
        healthcheck.enqueue_batch([{'args': [1], 'queue_name': self.db_id} for x in range(0, 40)])
        proc = multiprocessing.Process(target=worker.run)
        proc.start()
        time.sleep(1)
        logger.info('first term')
        proc.terminate()
        time.sleep(1)
        logger.info('second term')
        proc.terminate()
        logger.info('joining')
        proc.join()
        assert not proc.is_alive()

    def test_enqueue(self):
        Job.collection.delete_many({})
        worker = Worker(self.app, queue_name=self.db_id, greenlets=4, max_jobs=4, burst=True)
        worker.q.client.purge()
        job = healthcheck.enqueue(.01, queue_name=self.db_id)
        worker.run()
        job.reload()
        assert job.status == 'complete'
        assert job.result == 'Healthy after 0.01 seconds'

    def test_enqueue_at(self):
        one_minute_ahead = datetime.utcnow().replace(microsecond=0) + timedelta(minutes=1)
        job_schedule = healthcheck.enqueue_at(one_minute_ahead, .1, queue_name=self.db_id)
        assert job_schedule.next_runtime == one_minute_ahead

    def test_enqueue_in_schedule(self):
        job_schedule = healthcheck.enqueue_in(timedelta(minutes=30), .1, queue_name=self.db_id)
        assert job_schedule.next_runtime > datetime.utcnow() + timedelta(minutes=29)

    def test_enqueue_in_job(self):
        job = healthcheck.enqueue_in(timedelta(seconds=60), .1, queue_name=self.db_id)
        assert job.delay_seconds == 60

    def test_enqueue_debounce(self):
        healthcheck.enqueue_debounce(.1, delay=timedelta(minutes=5), queue_name=self.db_id)
        healthcheck.enqueue_debounce(.1, delay=timedelta(minutes=5), queue_name=self.db_id)
        healthcheck.enqueue_debounce(.1, delay=timedelta(minutes=5), queue_name=self.db_id)
        healthcheck.enqueue_debounce(.1, delay=timedelta(minutes=5), queue_name=self.db_id)
        now = datetime.utcnow()
        schedule = healthcheck.enqueue_debounce(.1, delay=timedelta(minutes=5), queue_name=self.db_id)
        assert JobSchedule.count_documents({
            'method_name': healthcheck.__name__,
            'module_name': healthcheck.__module__,
            'args': [.1],
        }) == 1
        assert schedule.next_runtime > now + timedelta(minutes=5)

    def test_enqueue_after(self):
        jobs = [healthcheck.enqueue(x, queue_name=self.db_id) for x in range(10)]
        job_ids = [j.id for j in jobs]
        now = datetime.utcnow()
        time.sleep(1)
        schedule = healthcheck.enqueue_after(job_ids, .1, queue_name=self.db_id)
        assert JobSchedule.count_documents({
            'method_name': healthcheck.__name__,
            'args': [.1],
            'depends_on': job_ids,
        }) == 1
        assert schedule.next_runtime > now + timedelta(minutes=2)

    def test_cron(self):
        one_minute_ahead = datetime.utcnow() + timedelta(minutes=1)
        job_schedule = healthcheck.cron('* * * * *', .1)
        assert job_schedule.cron_expression == '* * * * *'
        assert job_schedule.next_runtime < one_minute_ahead

    def test_run_sync(self):
        job = healthcheck.run_sync(.01)
        assert job.result == 'Healthy after 0.01 seconds'
        assert job.queue_name == 'sync'
        assert job.status == 'complete'

    def test_schedule_jobs_cron(self):
        job = healthcheck.enqueue(3, queue_name=self.db_id)
        healthcheck.cron('* * * * *', 5)
        healthcheck.enqueue_at(datetime.utcnow() - timedelta(minutes=5), 10)
        deps_schedule = healthcheck.enqueue_after([job.id], 4, queue_name=self.db_id)
        deps_schedule.next_runtime = datetime.utcnow()
        deps_schedule.commit()
        time.sleep(1)
        now = datetime.utcnow()
        this_minute = datetime(now.year, now.month, now.day, now.hour, now.minute)
        schedule_jobs(cron_run=True)
        assert Job.count_documents({'method_name': 'healthcheck', 'args': 5}) == 1
        assert Job.count_documents({'method_name': 'healthcheck', 'args': 10}) == 0
        schedule = JobSchedule.find_one({'method_name': 'healthcheck', 'cron_expression': '* * * * *'})
        assert schedule.next_runtime == this_minute + timedelta(minutes=1)
        # test that cron sched does not touch deps schedules
        assert JobSchedule.count_documents({'depends_on': job.id}) == 1
        assert Job.count_documents({'args': 4}) == 0

    def test_schedule_jobs_deps(self):
        job = healthcheck.enqueue(3, queue_name=self.db_id)
        schedule = healthcheck.enqueue_after([job.id], 4, queue_name=self.db_id)
        schedule.next_runtime = datetime.utcnow()
        schedule.commit()
        schedule_jobs(deps_run=True)
        assert Job.count_documents({'method_name': 'healthcheck', 'args': 4}) == 0
        assert JobSchedule.count_documents({'depends_on': job.id}) == 1
        schedule.next_runtime = datetime.utcnow()
        schedule.commit()
        job.run()
        schedule_jobs(deps_run=True)
        assert Job.count_documents({'method_name': 'healthcheck', 'args': 4}) == 1
        assert JobSchedule.count_documents({'depends_on': job.id}) == 0

    def test_schedule_jobs(self):
        job = healthcheck.enqueue(3, queue_name=self.db_id)
        healthcheck.cron('* * * * *', 5)
        healthcheck.enqueue_at(datetime.utcnow() - timedelta(minutes=5), 10, queue_name=self.db_id)
        deps_schedule = healthcheck.enqueue_after([job.id], 4, queue_name=self.db_id)
        deps_schedule.next_runtime = datetime.utcnow()
        deps_schedule.commit()
        time.sleep(1)
        schedule_jobs()
        assert Job.count_documents({'method_name': 'healthcheck', 'args': 5}) == 0
        assert Job.count_documents({'method_name': 'healthcheck', 'args': 10}) == 1
        assert JobSchedule.count_documents({'method_name': 'healthcheck', 'args': 10}) == 0
        assert JobSchedule.count_documents({'method_name': 'healthcheck', 'depends_on': job.id}) == 1
        assert Job.count_documents({'args': 4}) == 0

    def test_check_heartbeats(self):
        job = healthcheck.enqueue(20, queue_name=self.db_id)
        job.status = 'running'
        job.last_heartbeat = datetime.utcnow() - timedelta(minutes=2)
        job.commit()
        check_job_heartbeats()
        job.reload()
        assert job.status == 'failed'
        assert job.result == 'rip'
