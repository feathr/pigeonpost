import os
import uuid

import boto3
from bson import json_util
import botocore
from flask import current_app

from . import logger

queues = {}


class Queue:

    def __init__(self, queue_name):
        region_name = current_app.config.get('PIGEONPOST_SQS_REGION', os.getenv('AWS_REGION', 'us-east-1'))
        endpoint_url = None
        if current_app.testing:
            endpoint_url = 'http://sqs:9324/' if current_app.env == 'development' else 'http://localhost:9324/'
        if not queues.get(queue_name):
            self.connect(queue_name, region_name, endpoint_url)
            logger.info(f'connected to {queues.get(queue_name)}')
        self.client = queues.get(queue_name)

    def connect(self, queue_name, region_name, endpoint_url):
        self.region_name = region_name
        self.session = boto3.Session(region_name=region_name)
        self.config = botocore.config.Config(retries={'max_attempts': 8})
        self.resource = self.session.resource('sqs', config=self.config, endpoint_url=endpoint_url)
        try:
            queues[queue_name] = self.resource.get_queue_by_name(QueueName=queue_name)
        except botocore.exceptions.ClientError as e:
            if e.__class__.__name__ == 'QueueDoesNotExist':
                logger.warning(f'queue {queue_name} does not exist, creating...')
                queues[queue_name] = self.resource.create_queue(QueueName=queue_name)
            else:
                raise e

    def send_message(self, msg, delay_seconds=None):
        msg = json_util.dumps(msg)
        if delay_seconds:
            return self.client.send_message(MessageBody=msg, DelaySeconds=delay_seconds)
        return self.client.send_message(MessageBody=msg)

    def send_messages(self, entries):
        return self.client.send_messages(Entries=entries)

    def receive_messages(self, max_messages=1, wait_time_seconds=5):
        return self.client.receive_messages(MaxNumberOfMessages=max_messages, WaitTimeSeconds=wait_time_seconds)

    def delete_message(self, receipt_handle):
        return self.client.delete_messages(Entries=[{'Id': uuid.uuid4().hex, 'ReceiptHandle': receipt_handle}])
