from datetime import datetime, timedelta
from importlib import import_module
from bson import json_util
from pymongo.operations import UpdateOne
import threading
from traceback import format_exc
from umongo import PyMongoInstance

from croniter import croniter
from flask import current_app, g
from umongo import fields, Document, validate

from . import logger
from .queue import Queue

__all__ = ['Job', 'JobSchedule']

instance = PyMongoInstance()


class Heart(threading.Thread):

    def __init__(self, job_id, event):
        super(Heart, self).__init__()
        self.job_id = job_id
        self.stopped = event

    def run(self):
        while not self.stopped.is_set():
            self.beat()
            self.stopped.wait(1)
        self.beat()

    def beat(self):
        Job.collection.update_one({'_id': self.job_id}, {'$set': {'last_heartbeat': datetime.utcnow()}})


@instance.register
class Job(Document):

    class Meta:
        indexes = [
            'status',
            'method_name',
            ('module_name', 'method_name', 'next_runtime', 'args', 'kwargs'),
            {
                'key': ['expire_at'],
                'expireAfterSeconds': 0,
            }
        ]

    job_schedule = fields.ReferenceField('JobSchedule')
    message_id = fields.StringField()
    module_name = fields.StringField(required=True)
    method_name = fields.StringField(required=True)
    args = fields.ListField(fields.BaseField(allow_none=True))
    kwargs = fields.DictField()
    result = fields.BaseField()
    queue_name = fields.StringField()
    delay_seconds = fields.IntField()
    status = fields.StringField(default='enqueued', choices=['enqueued', 'running', 'complete', 'failed'])
    log = fields.ListField(fields.StringField())
    enqueued_at = fields.DateTimeField()
    started_at = fields.DateTimeField()
    ended_at = fields.DateTimeField()
    expire_at = fields.DateTimeField(default=lambda: datetime.utcnow() + timedelta(days=4))
    last_heartbeat = fields.DateTimeField()
    attempts = fields.IntField(default=0)
    depends_on = fields.ListField(fields.ReferenceField('Job'))

    def __str__(self):
        return f'<{self.pk}> {self.module_name}.{self.method_name}(*{self.args}, **{self.kwargs})'

    def runtime(self):
        ended_at = self.ended_at or datetime.utcnow()
        if not self.started_at:
            return 0.0
        return (ended_at - self.started_at).total_seconds()

    def enqueue(self):
        self.commit()
        try:
            q = Queue(self.queue_name)
            result = q.send_message(self.to_mongo(), delay_seconds=self.delay_seconds)
            self.message_id = result['MessageId']
            self.enqueued_at = datetime.utcnow()
        except Exception:
            self.status = 'failed'
            self.result = str(format_exc())
        self.commit()
        return self

    @classmethod
    def enqueue_batch(cls, batch, queue_name='low'):
        counter = 0
        q_counter = 0
        job_docs = []
        messages = []
        batched_job_ids = []
        enqueued_job_ids = []

        q = Queue(queue_name)
        while batch:
            counter += 1
            job_docs.append(batch.pop())
            if counter == 500 or not batch:
                logger.info(f'Batching {counter}, {len(batch)} left.')
                result = cls.collection.insert_many(job_docs)
                batched_job_ids += result.inserted_ids
                num_jobs_remaining = len(result.inserted_ids)
                for job in Job.find({'_id': {'$in': result.inserted_ids}}):
                    message_body = json_util.dumps(job.to_mongo())
                    messages.append({'Id': str(job.id), 'MessageBody': message_body})
                    enqueued_job_ids.append(job.id)
                    num_jobs_remaining -= 1
                    q_counter += 1
                    if q_counter == 10 or num_jobs_remaining == 0:
                        q.send_messages(entries=messages)
                        q_counter = 0
                        messages = []
                        cls.collection.update_many({'_id': {'$in': enqueued_job_ids}},
                                                   {'$set': {'enqueued_at': datetime.utcnow()}})
                        enqueued_job_ids = []
                counter = 0
                job_docs = []
        return batched_job_ids

    def run(self):
        if self.status == 'running':
            return 'running', 0
        self.status = 'running'
        self.started_at = datetime.utcnow()
        self.attempts += 1
        self.commit()
        stop_heartbeat = threading.Event()
        heart = Heart(self.id, stop_heartbeat)
        heart.start()
        try:
            with current_app.app_context():
                module = import_module(self.module_name)
                method = getattr(module, self.method_name)
                g.job = self
                args = self.args if self.args else list()
                kwargs = self.kwargs if self.kwargs else dict()
                result = method(*args, **kwargs)
            if result:
                self.result = result
            self.status = 'complete'
            self.expire_at = datetime.utcnow() + timedelta(hours=1)
        except Exception as e:
            logger.exception(e)
            self.status = 'failed'
            self.result = str(format_exc())
            self.expire_at = datetime.utcnow() + timedelta(days=4)
        self.ended_at = datetime.utcnow()
        try:
            self.commit()
        except Exception:
            return '???', self.runtime()
        stop_heartbeat.set()
        return self.status, self.runtime()


@instance.register
class JobSchedule(Document):

    cron_expression = fields.StringField()
    next_runtime = fields.DateTimeField(default=datetime.utcnow)
    last_runtime = fields.DateTimeField()
    module_name = fields.StringField(required=True)
    method_name = fields.StringField(required=True)
    args = fields.ListField(fields.BaseField(allow_none=True))
    kwargs = fields.DictField()
    queue_name = fields.StringField(required=True)
    depends_on = fields.ListField(fields.ReferenceField('Job'))
    dependency_mode = fields.StringField(default='STRICT', validate=validate.OneOf(['STRICT', 'LAX']))
    dependency_interval = fields.IntField(default=2)

    class Meta:
        indexes = [
            'method_name',
            'cron_expression',
            'next_runtime',
            ('method_name', 'cron_expression'),
            ('method_name', 'args'),
            ('method_name', 'kwargs'),
            ('method_name', 'args', 'kwargs'),
            ('module_name', 'method_name', 'next_runtime', 'args', 'kwargs'),
            ('next_runtime', 'cron_expression'),
        ]

    def __str__(self):
        return f'<{self.queue_name}> {self.module_name}.{self.method_name}( *{self.args}, **{self.kwargs})'

    def get_next_runtime(self):
        if not self.cron_expression:
            return self.next_runtime
        return croniter(self.cron_expression).get_next(datetime)

    def to_job(self):
        job = Job(
            job_schedule=self,
            queue_name=self.queue_name,
            module_name=self.module_name,
            method_name=self.method_name,
        )
        if self.args:
            job.args = self.args
        if self.kwargs:
            job.kwargs = self.kwargs
        if self.depends_on:
            job.depends_on = self.depends_on
        return job

    def check_dependencies(self):
        statuses = [j.status for j in Job.find({'id': {'$in': [ref.pk for ref in self.depends_on]}}, {'status': 1})]
        if len(statuses) != len(self.depends_on):
            return 'abort'
        if all([s in ['complete', 'failed'] for s in statuses]):
            if self.dependency_mode == 'STRICT' and not all([s in ['complete'] for s in statuses]):
                return 'abort'
            return 'ready'
        return 'wait'

    def wait_for_dependencies(self):
        return UpdateOne(
            {'_id': self.id},
            {'$set': {'next_runtime': datetime.utcnow() + timedelta(minutes=self.dependency_interval)}},
        )

    @classmethod
    def update_runtime(cls, sender, document, **kwargs):
        if document.cron_expression:
            document.next_runtime = document.get_next_runtime()
