import click
from importlib import import_module
import types


def get_app(module):
    parts = module.split(":", 1)
    attr = 'app'
    if len(parts) == 2:
        module, attr = parts[0], parts[1]
    app = getattr(import_module(module), attr)
    if isinstance(app, types.FunctionType):
        app = app()
    return app


@click.group()
def pigeonpost():
    pass


@pigeonpost.command()
@click.argument('module', type=str)
@click.argument('queue-name', type=str)
@click.option('-g', '--greenlets', type=int, default=30)
@click.option('-j', '--max-jobs', type=int, default=1000)
@click.option('-b', '--burst', type=bool, default=False)
def worker(module, queue_name, greenlets, max_jobs, burst):
    import gevent.monkey
    gevent.monkey.patch_all()
    from .worker import Worker
    app = get_app(module)
    worker = Worker(app, queue_name, greenlets=greenlets, max_jobs=max_jobs, burst=burst)
    worker.run()


@pigeonpost.command()
@click.argument('module', type=str)
def scheduler(module):
    from .jobs import schedule_jobs, check_job_heartbeats
    app = get_app(module)
    with app.app_context():
        schedule_jobs.enqueue_debounce(cron_run=True)
        schedule_jobs.enqueue_debounce(deps_run=True)
        schedule_jobs.enqueue_debounce()
        check_job_heartbeats.enqueue_debounce()
