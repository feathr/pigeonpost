import logging

from flask import g


class JobLogHandler(logging.Handler):
    def __init__(self, job, *args, **kwargs):
        self.job = job
        super(JobLogHandler, self).__init__(*args, **kwargs)

    def emit(self, record):
        log_entry = self.format(record)
        return self.job.log.append(log_entry)


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
sh = logging.StreamHandler()
sh.setLevel(logging.INFO)

# create formatter
formatter = logging.Formatter('[%(asctime)s] %(levelname)s in %(module)s: %(message)s')

# add formatter to sh
sh.setFormatter(formatter)
if g and g.job:
    job_handler = JobLogHandler(g.job)
    job_handler.setFormatter(formatter)
    logger.addHandler(job_handler)

# add sh to logger
logger.addHandler(sh)
