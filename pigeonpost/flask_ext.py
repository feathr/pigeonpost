from pymongo import MongoClient

from .models import instance


class PigeonPost:

    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        mongo_client = MongoClient(app.config.get('PIGEONPOST_MONGO_URI'), connect=False)
        instance.init(mongo_client[app.config.get('PIGEONPOST_MONGO_DB')])
