from time import sleep
from datetime import datetime, timedelta

from .models import Job, JobSchedule


class job:

    def __init__(self, queue_name='low', region_name='us-east-1', delay_seconds=None):
        self.queue_name = queue_name
        self.region_name = region_name
        self.delay_seconds = delay_seconds

    def __call__(self, f):
        def _create_job_schedule(queue_name, next_runtime=None, **kwargs):
            job_schedule = JobSchedule(module_name=f.__module__, method_name=f.__name__, queue_name=queue_name)
            if next_runtime:
                job_schedule.next_runtime = next_runtime
            if kwargs.get('args'):
                job_schedule.args = kwargs.get('args')
            if kwargs.get('kwargs'):
                job_schedule.kwargs = kwargs.get('kwargs')
            if kwargs.get('cron_expression'):
                job_schedule.cron_expression = kwargs.get('cron_expression')
            if kwargs.get('depends_on'):
                job_schedule.depends_on = kwargs.get('depends_on')
            if kwargs.get('dependency_interval'):
                job_schedule.dependency_interval = kwargs.get('dependency_interval')
            if kwargs.get('dependency_mode'):
                job_schedule.dependency_mode = kwargs.get('dependency_mode')
            job_schedule.commit()
            return job_schedule

        def enqueue(*args, **kwargs):
            queue_name = kwargs.pop('queue_name', self.queue_name)
            delay_seconds = kwargs.pop('delay_seconds', self.delay_seconds)
            job = Job(module_name=f.__module__, method_name=f.__name__, queue_name=queue_name)
            if args:
                job.args = args
            if kwargs:
                job.kwargs = kwargs
            if delay_seconds:
                job.delay_seconds = delay_seconds
            return job.enqueue()

        def enqueue_batch(batch):
            jobs = []
            for params in batch:
                args = params.get('args')
                kwargs = params.get('kwargs')
                delay_seconds = params.get('delay_seconds', self.delay_seconds)
                queue_name = params.get('queue_name', self.queue_name)

                job = Job(**{
                    'module_name': f.__module__,
                    'method_name': f.__name__,
                    'queue_name': queue_name,
                })
                if delay_seconds:
                    job.delay_seconds = delay_seconds
                if args:
                    job.args = args
                if kwargs:
                    job.kwargs = kwargs
                jobs.append(job.dump())

            batch_dict = {}
            for job in jobs:
                queue_name = job.get('queue_name')
                queue_jobs = batch_dict.get(queue_name, [])
                batch_dict[queue_name] = queue_jobs + [job]

            inserted_ids = []
            for k, v in batch_dict.items():
                inserted_ids.append(Job.enqueue_batch(v, queue_name=k))
            return inserted_ids

        def enqueue_throttle(batch, max_jobs=3):
            running_job_ids = []
            running_job_count = 0
            running_job_query = {'_id': {'$in': running_job_ids}, 'status': {'$in': ('running', 'enqueued')}}
            for params in batch:
                while running_job_count >= max_jobs:
                    sleep(3)
                    running_job_count = Job.collection.count_documents(running_job_query)

                args = params.get('args', [])
                kwargs = params.get('kwargs', {})

                job = f.enqueue(*args, **kwargs)
                running_job_ids.append(job.id)
                running_job_count += 1

            while running_job_count:
                sleep(3)
                running_job_count = Job.collection.count_documents(running_job_query)

            return running_job_ids

        def enqueue_at(next_runtime, *args, **kwargs):
            queue_name = kwargs.pop('queue_name', self.queue_name)
            return _create_job_schedule(queue_name, next_runtime=next_runtime, args=args, kwargs=kwargs)

        def enqueue_in(runtime_delta, *args, **kwargs):
            if runtime_delta.total_seconds() <= 900:
                kwargs['delay_seconds'] = runtime_delta.seconds
                return f.enqueue(*args, **kwargs)
            next_runtime = datetime.utcnow() + runtime_delta
            queue_name = kwargs.pop('queue_name', self.queue_name)
            return _create_job_schedule(queue_name, next_runtime, args=args, kwargs=kwargs)

        def enqueue_debounce(*args, delay=None, **kwargs):
            queue_name = kwargs.pop('queue_name', self.queue_name)
            job_query_doc = {
                'method_name': f.__name__,
                'module_name': f.__module__,
                'status': {'$in': ['running', 'enqueued']},
                'args': args if args else {'$exists': False},
                'kwargs': kwargs if kwargs else {'$exists': False}
            }
            existing_job = Job.find_one(job_query_doc)
            if existing_job:
                return existing_job
            if delay:
                schedule_query_doc = {
                    'method_name': f.__name__,
                    'module_name': f.__module__,
                    'next_runtime': {'$gte': datetime.utcnow()},
                    'args': args if args else {'$exists': False},
                    'kwargs': kwargs if kwargs else {'$exists': False}
                }
                existing_schedule = JobSchedule.find_one(schedule_query_doc)
                if existing_schedule:
                    existing_schedule.next_runtime = datetime.utcnow() + delay
                    existing_schedule.commit()
                    return existing_schedule
                next_runtime = datetime.utcnow() + delay
                return _create_job_schedule(queue_name, next_runtime, args=args, kwargs=kwargs)

            return f.enqueue(*args, **kwargs)

        def enqueue_after(job_ids, *args, dependency_mode='STRICT', dependency_interval=2, **kwargs):
            queue_name = kwargs.pop('queue_name', self.queue_name)
            next_runtime = datetime.utcnow() + timedelta(minutes=dependency_interval)
            return _create_job_schedule(
                queue_name,
                next_runtime,
                depends_on=job_ids,
                dependency_mode=dependency_mode,
                dependency_interval=dependency_interval,
                args=args,
                kwargs=kwargs)

        def cron(cron_expression, *args, **kwargs):
            queue_name = kwargs.pop('queue_name', self.queue_name)
            return _create_job_schedule(queue_name, cron_expression=cron_expression, args=args, kwargs=kwargs)

        def run_sync(*args, **kwargs):
            job = Job(
                method_name=f.__name__,
                module_name=f.__module__,
                enqueued_at=datetime.utcnow(),
                queue_name='sync',
            )
            if args:
                job.args = args
            if kwargs:
                job.kwargs = kwargs
            job.commit()
            job.run()
            job.reload()
            return job

        def get_job_cursor(query={}, projection=None):
            query = {'method_name': f.__name__, 'module_name': f.__module__, **query}
            return Job.find(query, projection=projection)

        def get_job_schedule_cursor():
            return JobSchedule.find({'method_name': f.__name__, 'module_name': f.__module__})

        def get_latest_job():
            return f.get_job_cursor().sort('_id', -1)[0]

        def num_jobs():
            return Job.collection.count_documents({'module_name': f.__module__, 'method_name': f.__name__})

        def num_complete_jobs():
            query = {'module_name': f.__module__, 'method_name': f.__name__, 'status': 'complete'}
            return Job.collection.count_documents(query)

        def num_failed_jobs():
            query = {'module_name': f.__module__, 'method_name': f.__name__, 'status': 'failed'}
            return Job.collection.count_documents(query)

        f.enqueue = enqueue
        f.enqueue_at = enqueue_at
        f.enqueue_in = enqueue_in
        f.enqueue_debounce = enqueue_debounce
        f.enqueue_after = enqueue_after
        f.cron = cron
        f.run_sync = run_sync
        f.enqueue_batch = enqueue_batch
        f.enqueue_throttle = enqueue_throttle
        f.get_job_cursor = get_job_cursor
        f.get_job_schedule_cursor = get_job_schedule_cursor
        f.get_latest_job = get_latest_job
        f.num_jobs = num_jobs
        f.num_complete_jobs = num_complete_jobs
        f.num_failed_jobs = num_failed_jobs

        return f
