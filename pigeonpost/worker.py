import multiprocessing
import time

from bson import json_util
from gevent.pool import Pool
from gevent import GreenletExit, signal

from . import logger
from .models import Job
from .queue import Queue

__all__ = ['Worker']


class Worker:

    def __init__(self, app, queue_name='default', burst=False, timeout_seconds=None, greenlets=50, max_jobs=1000):

        self.timeout_seconds = timeout_seconds
        self.greenlets = greenlets
        self.max_jobs = max_jobs
        self.burst = burst
        self.consumer_drain = []
        self.app = app

        self.total_jobs_complete = multiprocessing.Value('i', 0)
        self.total_jobs_failed = multiprocessing.Value('i', 0)

        self.shutdown_event = multiprocessing.Event()
        self.started_at = None
        self.last_health_check = 0
        self._last_health_check_log = None
        self._closed = False

        with self.app.app_context():
            self.q = Queue(queue_name)

    @property
    def runtime(self):
        return time.time() - self.started_at

    def run(self):
        self.started_at = time.time()
        self.spawn_consumer()
        self.set_signal_handlers()

        while not self.shutdown_event.is_set():
            self.monitor_consumer()
            time.sleep(1)

        self.drain_consumer()
        logger.info(self.consumer_drain)
        for consumer in self.consumer_drain:
            consumer.join()

    def clean_consumer_drain(self):
        self.consumer_drain = [c for c in self.consumer_drain if c.is_alive()]

    def drain_consumer(self):
        self.consumer.shutdown_event.set()
        self.consumer_drain.append(self.consumer)
        self.clean_consumer_drain()

    def monitor_consumer(self):
        if self.shutdown_event.is_set():
            self.consumer.shutdown_event.set()
        if self.consumer.shutdown_event.is_set():
            self.drain_consumer()
            self.spawn_consumer()

    def spawn_consumer(self):
        if not self.shutdown_event.is_set():
            self.consumer = Consumer(self)
            self.consumer.daemon = True
            self.consumer.start()

    def heart_beat(self):
        logger.info('healthy')
        time.sleep(10)

    def shutdown(self):
        self.shutdown_event.set()
        t = time.time() - self.started_at if self.started_at else 0
        logger.info(f'shutting down worker after {t}s ◆ '
                    f'{self.total_jobs_complete.value} jobs complete ◆ '
                    f'{self.total_jobs_failed.value} failed ◆ '
                    f'{self.total_jobs_complete.value / self.runtime} jobs/sec')

    def set_signal_handlers(self):
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)

    def signal_handler(self, sig, frame):
        logger.info(f'got signal: {signal.Signals(sig).name}, stopping...')
        self.shutdown()
        self.consumer.terminate()
        signal.signal(signal.SIGINT, self.immediate_exit)
        signal.signal(signal.SIGTERM, self.immediate_exit)

    def immediate_exit(self, sig, frame):
        logger.warning(f'received second signal: {signal.Signals(sig).name}, immediately exiting...')
        exit(1)


class Consumer(multiprocessing.Process):

    def __init__(self, worker):
        super().__init__()
        self.jobs_complete = 0
        self.jobs_failed = 0
        self.shutdown_event = multiprocessing.Event()
        self.worker = worker
        self.pool = Pool(size=self.worker.greenlets)

    def poll(self, max_messages=10):
        logger.info('polling')
        return self.worker.q.receive_messages(max_messages=max_messages)

    def increment_job_counter(self, status):
        if status == 'complete':
            self.jobs_complete += 1
            with self.worker.total_jobs_complete.get_lock():
                self.worker.total_jobs_complete.value += 1
        elif status == 'failed':
            self.jobs_failed += 1
            with self.worker.total_jobs_failed.get_lock():
                self.worker.total_jobs_failed.value += 1

    def should_shutdown(self):
        return self.jobs_complete + self.jobs_failed >= self.worker.max_jobs

    def run_job(self, message):
        with self.worker.app.test_request_context():
            job = Job()
            try:
                job_dict = json_util.loads(message.body)
                job.from_mongo(job_dict)
                job_status, job_runtime = job.run()
                self.increment_job_counter(job_status)
            except Exception as exc:
                logger.exception(exc)
            finally:
                if self.should_shutdown():
                    self.shutdown_event.set()
            return message.receipt_handle

    def delete_message(self, gl):
        if isinstance(gl.value, GreenletExit):
            return
        delete_result = self.worker.q.delete_message(receipt_handle=gl.value)
        if not delete_result.get('ResponseMetadata', {}).get('HTTPStatusCode') == 200:
            logger.error(delete_result)

    def signal_handler(self, sig, frame):
        logger.info('terminating consumer')
        self.shutdown_event.set()
        signal.signal(signal.SIGINT, self.immediate_exit)
        signal.signal(signal.SIGTERM, self.immediate_exit)

    def immediate_exit(self, sig, frame):
        logger.warning(f'<{self.pid}>received second signal: killing pool')

    def run(self):
        logger.info(f'starting consumer: {self.name}')
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)

        while not self.shutdown_event.is_set():
            messages = self.poll(10)
            if not messages and self.worker.burst:
                self.worker.shutdown_event.set()
                self.shutdown_event.set()
            for message in messages:
                gt = self.pool.spawn(self.run_job, message)
                gt.link_value(self.delete_message)

        logger.info(f'<{self.pid}> awaiting {len(self.pool)} jobs')
        self.pool.join()
        logger.info(f'<{self.pid}> exiting')
