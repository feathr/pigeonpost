from datetime import datetime, timedelta

from . import logger
from .decorators import job
from .models import JobSchedule, Job


@job('high')
def schedule_jobs(cron_run=False, deps_run=False, limit=10000):
    batch_dict = {}
    sched_updates = []
    query = {
        'next_runtime': {'$lte': datetime.utcnow()},
        'cron_expression': None,
        'depends_on': None,
    }

    if cron_run:
        query.update({'cron_expression': {'$ne': None}})
    elif deps_run:
        query.update({'depends_on': {'$ne': None}})

    if not JobSchedule.collection.count_documents(query):
        if cron_run:
            logger.info('No cron jobs to schedule.')
        else:
            logger.info('No onerun jobs to schedule.')
        return

    job_sched_ids = []
    for job_sched in JobSchedule.find(query, limit=limit):
        if deps_run:
            result = job_sched.check_dependencies()
            if result == 'wait':
                sched_updates.append(job_sched.wait_for_dependencies())
                continue
            elif result == 'abort':
                job_sched_ids.append(job_sched.id)
                continue
        job_sched_ids.append(job_sched.id)
        job = job_sched.to_job()
        batch = batch_dict.get(job_sched.queue_name, [])
        batch_dict[job_sched.queue_name] = batch + [job.dump()]
        if cron_run:
            job_sched.next_runtime = job_sched.get_next_runtime()
            job_sched.last_runtime = datetime.utcnow()
            job_sched.commit()

    job_ids = []
    for k, v in batch_dict.items():
        job_ids += Job.enqueue_batch(v, queue_name=k)

    logger.info(f'{len(job_ids)} jobs enqueued.')
    if len(sched_updates):
        JobSchedule.collection.bulk_write(sched_updates)
    if not cron_run:
        logger.info('Deleting job schedules.')
        JobSchedule.collection.delete_many({'_id': {'$in': job_sched_ids}})
    return len(job_ids)


@job('high')
def check_job_heartbeats():
    query = {'status': 'running', 'last_heartbeat': {'$lt': datetime.utcnow() - timedelta(minutes=1)}}
    update = {'$set': {'status': 'failed', 'result': 'rip'}}
    Job.collection.update_many(query, update)
