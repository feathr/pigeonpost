import sys

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to pytest")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = ''

    def run_tests(self):
        import shlex
        import pytest
        from gevent import monkey
        monkey.patch_all()
        errno = pytest.main(shlex.split(self.pytest_args))
        sys.exit(errno)


setup(
    name='pigeonpost',
    version='0.1.0',
    packages=find_packages(),
    license='MIT',
    description='Async job library for Flask using mongodb and sqs.',
    url='https://bitbucket.org/feathr/pigeonpost/',
    author='Otis Stamp',
    author_email='otistamp@gmail.com',
    include_package_data=True,
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    install_requires=[
        'Click==7.1.1',
        'boto3==1.10.39',
        'croniter==0.3.31',
        'Flask==1.1.1',
        'gevent==1.5.0',
        'pymongo==3.10.1',
        'umongo==2.2.0',
        'werkzeug==1.0.0',
    ],
    cmdclass={'test': PyTest},
    tests_require=[
        'flake8==3.7.9',
        'ipdb==0.12.3',
        'ipython==7.12.0',
        'pytest==5.3.2',
        'pytest-cov==2.8.1',
    ],
    entry_points='''
        [console_scripts]
        pigeonpost=pigeonpost.cli:pigeonpost
    ''',
)
