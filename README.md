pigeonpost
=============

Async job library for Flask using mongodb and sqs.

## Installation

`pip install pigeonpost`

## Setup

### Mongo

Simply set `PIGEONPOST_MONGO_URI` (full connection uri) and `PIGEONPOST_MONGO_DB` (db name) in your flask app config.

### SQS

Set `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` environment variables

### Flask

Instantiate the flask extension with your flask app:
```python
from pigeonpost.flask_ext import PigeonPost

PigeonPost(app)
```

## Usage


### Decorator

```python
from pigeonpost.decorators import job

@job('default')
def pigeonpost_job(*args, **kwargs):
    return 'hello, world!'

# send it off for async processing on the default queue
pigeonpost_job.enqueue('arg', kwarg='kwarg')

# enqueue at a specific time in the future
pigeonpost_job.enqueue_at(datetime(2029, 12, 31), 'arg', kwarg='kwarg')

# enqueue in the future
pigeonpost_job.enqueue_in(timedelta(days=1), 'arg', kwarg='kwarg')

# call it syncronously
pigeonpost_job()
```

### Commands

`pigeonpost worker path.to.app:create_apps queue_name`
`pigeonpost scheduler`
